<small>[HOME](https://osint.miraheze.org/wiki/Main_Page) \> [Social
Media](https://osint.miraheze.org/wiki/SOCMINT) \> Creating
Accounts</small>


Creating social media accounts to use in your research is a critical
part of many OSINT toolboxes.


\=== Creating Sock Puppets ===

  - [Creating SM Profiles](SOCMINT_Profile_Data "wikilink")
  - [Creating Profile Pics](SOCMINT_Profile_Pics "wikilink")
  - [Disposible Email, Email Forwarding, Public
    Email](SOCMINT_Email_Accounts "wikilink")
  - [SMS Accounts](SOCMINT_SMS_Accounts "wikilink")


\=== Scripts ===

  -

\=== Command line, APIs ===

  -

\=== Plugins and Extensions ===

  -

\=== Bookmarklets ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).