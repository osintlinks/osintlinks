<small>[HOME](https://osint.miraheze.org/wiki/Main_Page) \> [Social
Media](SOCMINT "wikilink") \> Social Media Profiles</small>


Resources to help with creating those social media profiles.


\== Websites ==

  - [fakenamegenerator.com](https://www.fakenamegenerator.com)
  - [elfqrin.com](https://www.elfqrin.com/fakeid.php)


\== Scripts ==

  - [faker.js](https://github.com/Marak/faker.js) NPM


\== Command line, APIs ==

  - NameFake API (create random id): `$ curl
    "`<https://api.namefake.com/random/random>`"` - no account needed
  - NameFake API (more specific id): `$ curl
    "`<https://api.namefake.com/ukrainian-ukraine/female/>`"`


\=== Plugins and Extensions ===

  -

\=== Bookmarklets ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).