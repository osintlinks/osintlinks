Searching for people is a common task, and we often start with some
identifier like an email address or a username. Please put links in the
child pages linked to below instead of here.

  - [search by name](People_Search_by_Name.md)
  - [search by email address](People_Search_by_Email.md)
  - [search by username](People_Search_by_Username.md)
  - [search by face / profile pic /
    avatar](People_Search_by_Face.md)

If you have comments or suggestions to improve this page, please reach
out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).