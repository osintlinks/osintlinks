# People Search

Searching for people is a common task, and we often start with some
identifier like an email address or a username. Please put links in the
child pages linked to below instead of here.

  - [search by name](people:Search_by_Name "wikilink")
  - [search by email address](people:Search_by_Email "wikilink")
  - [search by username](people:Search_by_Username "wikilink")
  - [search by face / profile pic /
    avatar](people:Search_by_Face "wikilink")

If you have comments or suggestions to improve this page, you can reach
out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).
