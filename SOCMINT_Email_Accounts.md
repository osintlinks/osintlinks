<small>[HOME](Main_Page "wikilink") \> [Social
Media](SOCMINT "wikilink") \> [Creating
Socks](SOCMINT_Creating_Socks "wikilink") \> Email Accounts</small>


Create temporary, disposible, public or fowarding email accounts to use
for new account signups.


\== Websites ==

  - [1](https://mailsac.com%7Cmailsac.com)\]


\== Scripts ==

  -

\== Command line, APIs ==

  -

\=== Plugins and Extensions ===

  -

\=== Bookmarklets ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).