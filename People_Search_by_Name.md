<small>[HOME](https://osint.miraheze.org/wiki/Main_Page) \> [People
Search](https://osint.miraheze.org/wiki/People_Search) \> By
Name</small>


Searching for people by name is a common task. There are often
challenges like common names that make this more difficult.


\=== Websites ===

  - [truepeoplesearch.com](https://truepeoplesearch.com)


\=== Scripts ===

  - [maigret](https://github.com/soxoj/maigret) great search tool from
    soxoj based on sherlock


\=== Command line, API ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).