<small>[HOME](https://osint.miraheze.org/wiki/Main_Page) \> [DNS,
Domains, IPs,
Websites](https://osint.miraheze.org/wiki/DNS_IPs_Domains_Websites) \>
IP, BGP Utils</small>


Searching for current and historical **DNS** data can reveal information
about domain names including hosting preferences, DNS providers used,
content delivery networks used, email services used, subdomains used and
services made available via those subdomains, analytics services used,
etc. Here are some OSINT resources for investigating DNS data.

Please add links to resources here only if you think they're as good or
better than what's already here, or have something to offer that the
existing ones do not.


\=== Websites ===

  - [thatsthem.com](https://thatsthem.com/reverse-ip-lookup) - reverse
    lookup
  - [viewDNS.info ASN lookup](https://viewdns.info/asnlookup/) -
    Autonomous System Number (ASN)
  - [viewDNS.info reverse IP lookup](https://viewdns.info/reverseip/)
  - [spyse.com/target/ip/9.9.9.9](https://spyse.com/target/ip/9.9.9.9) -
    substitute target IP address in this URL


\=== Scripts ===

  -

\=== Command line, APIs ===

  - `$ curl ipinfo.io/9.9.9.9`
  - viewDNS API: `curl
    "`<https://api.viewdns.info/reverseip/?host=9.9.9.9&apikey=YOUR_API_KEY&output=json>`"`
    - free account required


\=== Bookmarklets ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).