<small>[HOME](https://osint.miraheze.org/wiki/Main_Page) \> [People
Search](https://osint.miraheze.org/wiki/People_Search) \> By
Email</small>


Searching for people by email is a common task. But is the email address
valid or just a disposable?


\=== Websites ===

  - [truepeoplesearch.com](https://truepeoplesearch.com)
  - [emailsherlock.com](https://emailsherlock.com)


\=== Scripts ===

  -

\=== Command line, API ===

  - `curl emailrep.io/user@example.com` - get information about this
    email address
  - mailboxlayer.com API: `curl
    "`<https://apilayer.net/api/check?access_key=YOUR_API_KEY&email=user@example.com&smtp=1&format=1>`"`
    - free signup, 250 requests/month


\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).