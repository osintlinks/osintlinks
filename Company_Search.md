## Company Search

Searching for information about companies is a common task, and we often
start with some identifier like a company name and lookup business
registration data, corporate filings, legal proceedings involving that
company, employees and their feedback, etc. Please put links in the
child pages linked to below instead of here.

  - [search business registration
    sites](company:Business_Registrations "wikilink")
  - [company employees](company:Employees "wikilink")
  - [misc company data](company:Misc "wikilink")

If you have comments or suggestions to improve this page, you can reach
out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).