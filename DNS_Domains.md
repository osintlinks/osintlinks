Searching for current and historical **DNS** data can reveal information
about domain names including hosting preferences, DNS providers used,
content delivery networks used, email services used, subdomains used and
services made available via those subdomains, analytics services used,
etc. Here are some OSINT resources for investigating DNS data.

Please add links to resources here only if you think they're as good or
better than what's already here, or offer something that the existing
ones do not.


\=== DNS lookup websites ===

  - [whoisology.com](https://whoisology.com) - WHOIS lookup
  - <https://osint.sh/reversewhois/> - reverse WHOIS (find WHOIS recs
    matching an email address)
  - [web.archive.org](https://web.archive.org/web/2010*/https://who.is/example.com)
    - historical, replace `example.com` with target domain name


\=== WHOIS scripts, APIs, command line ===

  - `$ dig example.com` - fetch basic recs for example.com
  - `$ dig @12.34.56.78 example.com` - query 12.23.45.67, typically used
    to ask authoritative nameservers
  - `$ dig MX example.com` - fetch mail exchange records, use NS
    (nameservers), CNAME (aliases), etc.


[Return to Main Page](https://osint.miraheze.org/wiki/Main_Page)


\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).