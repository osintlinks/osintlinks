<small>[HOME](Main_Page "wikilink") \> [Social
Media](SOCMINT "wikilink") \> [Creating
Socks](SOCMINT_Creating_Socks "wikilink") \> SMS Accounts</small>


SMS is often required when creating new accounts.


\== Websites ==

  - [textverified.com](https://textverified.com)


\== Scripts ==

  -

\== Command line, APIs ==

  -

\=== Plugins and Extensions ===

  -

\=== Bookmarklets ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).