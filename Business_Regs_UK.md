<small>[HOME](Main_Page "wikilink") \> [Company
Search](Company_Search "wikilink") \> [By
Country](Business_Regs_by_Country "wikilink") \> Canada</small>


Search busines registrations in the UK.


\=== Websites ===

  - [Companies
    House](https://find-and-update-company-information.service.gov.uk)


\=== Scripts ===

  -

\=== Command line, APIs ===

  -

\=== Plugins and Extensions ===

  -

\=== Bookmarklets ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).