Searching for current and historical **WHOIS** data can reveal contact
information about domain name **registrants** including names, street
addresses, phone numbers, organizational details, hosting
choices/preferences and more. Here are some OSINT resources for
investigating WHOIS data.

Please add links to resources here only if you think they're as good or
better than what's already here, or offer something that the existing
ones do not.


\=== WHOIS lookup websites ===

  - [whoisology.com](https://whoisology.com) - WHOIS lookup
  - <https://osint.sh/reversewhois/> - reverse WHOIS (find WHOIS recs
    matching an email address)
  - [web.archive.org](https://web.archive.org/web/2010*/https://who.is/example.com)
    - historical, replace `example.com` with target domain name


\=== WHOIS scripts, APIs, command line ===

  - `$ whois example.com`


\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).