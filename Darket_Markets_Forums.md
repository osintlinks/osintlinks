<small>[HOME](https://osint.miraheze.org/wiki/Main_Page) \> Darknet
Markets and Forums</small>


Here are resources for investigating darknet markets, forums, breach
data (not recent)


\* [Tor Search Engines and Metasearch](Tor_Search "wikilink")

  - [Onion Sites of Interest](Onion_Sites "wikilink")
  - [Darknet Markets](Darknet_Markets "wikilink")
  - [Darknet Research Scripts, APIs, Sites](Darknet_Research "wikilink")
  - [Tor Clearnet Sites About Tor](Tor_Meta "wikilink")
  - [i2p, Zeronet, Freenet, and
    Others](Other_Anonymity_Networks "wikilink")


\=== Articles and References ===

  -

\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).