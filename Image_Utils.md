<small>[HOME](https://osint.miraheze.org/wiki/Main_Page) \> [Images and
Video](https://osint.miraheze.org/wiki/Images_Video) \> Image
Utilities</small>


Here are various utilities for manipulating and working with images.


\=== Websites ===

  - [OCR Text Extractor](https://osint.sh/ocr/)
  - [Online barcode
    reader](https://online-barcode-reader.inliteresearch.com)


\=== Scripts ===

  - [gallery-dl](https://github.com/mikf/gallery-dl) - download image
    collections from [various
    sources](https://github.com/mikf/gallery-dl/blob/master/docs/supportedsites.md)


\=== Command line, API ===

  -

\=== Plugins and Extensions ===

  -

\=== Bookmarklets ===

  -

\=== Articles ===

  - [Reverse Image Search Guide
    (Bellingcat)](https://bellingcat.com/resources/how-tos/2019/12/26/guide-to-using-reverse-image-search-for-investigations/)


\---- If you have comments or suggestions to improve this page, please
reach out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).