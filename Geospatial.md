Searching for geospatial or geolocation data is a common task, and we
often start with some identifier like a photograph or video and look at
satellite imagery or maps to identify sun positions and shadows, trees,
mountains, landscapes, etc. We also might be interested in various
webcams posted in well-known locations, Internet of Things searches,
Wi-Fi access points and more. Please put links in the child pages linked
to below instead of here.


\* [Satellite imagery, maps, sun, moon, mountains,
trees](Satellite_Imagery_and_Maps "wikilink")

  - [Buildings and real estate](Buildings_and_Real_Estate "wikilink")
  - [Location specific webcams](Webcams "wikilink")
  - [IoT, Wifi, etc.](IoT_Wifi "wikilink")


[Return to Main Page](https://osint.miraheze.org/wiki/Main_Page)


If you have comments or suggestions to improve this page, please reach
out to us on Matrix at
[\#osint-chat:matrix.org](https://matrix.to/#/#osint-chat:matrix.org) or
on Telegram at [@tmio_osint](https://t.me/tmio_osint).